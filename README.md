# Project : Genome assembly

### Author : Gautier COUTURE

## Pre-requisites
Requires Biopyhton to function correctly :
```
$ pip install Biopython
```
## Usage :
- Windows :
    ```
    $ py Assembly.py -h
    ```
- Linux :
    ```
    $ python3 Assembly.py -h
    ```

## Program Settings :
-h, --help      show this help message and exit <br />
-d , --dataset    Path to a fasta file that contain reads to assemble <br />
-e , --errors     Indicates whether the given dataset contain sequencing errors or not (0: No, 1: Yes) <br />
-o , --out        Output file name (& path) <br />
-k , --kmersize   Size of kmers we need (Optional, Default : 10)

## Exemple:
```
$ python3 Assembly.py -d DatasetAssembly/Sequencage1.fa -e 1 -o output.fa -k 25
```
## Implemented features :
- K-mer Index
- Reconstruction of the reference using sequencing data without errors
- K-mer filter (Low occurences)

## Unfinished features :
- Graph cleaning

## Non Implemented features :
- Reconstruction of the reference using sequencing data with errors
